# HydraMath

Mathematica program written in Wolfram Language to simplify te process of analyzing data from Time Correlated Single Photo Counting method.
PRogram converts the data from HydraHarp measuring computer program and then tries to find the best fit using numeric methods.

## Images of program
<img src="https://gitlab.com/avan1235/hydramath/raw/master/img/m1.png" width=750></img>
<img src="https://gitlab.com/avan1235/hydramath/raw/master/img/m2.png" width=750></img>